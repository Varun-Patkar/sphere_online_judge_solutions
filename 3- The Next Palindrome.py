def specialCase(n, i):
    if n == n[::-1]:
        return(n)
    else:
        n[i] += 1
        n = specialCase(n, i)
    return(n)


def do(n, n1):
    for i in range((len(n)//2)+1):
        if(n[i] != n[-(i+1)]):
            n[-(i+1)] = n[i]
        if((i == (len(n)//2))and(len(n) % 2 != 0)):
            n = specialCase(n, i)
        if((i == (len(n)//2)) and (n1 == n) and (len(n) % 2 != 0)):
            n[i] = str(int(n[i])+1)
    if((int(str(n))) < (int(str(n1)))):
        n = do(n, n1)  # Incomplete


n = list(str(int(input())))
n1 = n

print(*n, sep="")
